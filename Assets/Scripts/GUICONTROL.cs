using UnityEditor;
using UnityEngine;

public class GUICONTROL : MonoBehaviour
{


    private void OnGUI()
    {
        if (GUILayout.Button("Custom Window"))
        {
            StyleViewer myWindow = (StyleViewer)EditorWindow.GetWindow(typeof(StyleViewer));
            myWindow.Show();
        }
    }
}
