using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class TeleportTool : MonoBehaviour
{
    [SerializeField] private GUIStyle myButton;
    [SerializeField] private string[] locations;

    private void OnGUI()
    {
        var rect = GUILayoutUtility.GetRect(new GUIContent("Trop long pour le mot Button bonjourbjorjorjorjorj"), myButton);
        //if(GUI.Button(new Rect(0, 0, 100, 30), "myButton", myButton))
        if(GUI.Button(rect, "Button", myButton))
        {
            GUI.skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Scene);
            var dropdown = new DropDownExample(new AdvancedDropdownState(), locations);
            var rectMain = EditorGUIUtility.GetMainWindowPosition();
            dropdown.Show(new Rect(rectMain.center.x, rectMain.center.y, 300, 0));
        }
    }
}
