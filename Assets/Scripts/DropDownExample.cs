using System.Collections;
using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class DropDownExample : AdvancedDropdown
{
    private string[] locations;
    public DropDownExample(AdvancedDropdownState state, string[] loc) : base(state)
    {
        locations = loc;
        minimumSize = new Vector2(300, loc.Length * 150);
    }

    protected override AdvancedDropdownItem BuildRoot()
    {
        var root = new AdvancedDropdownItem("Le nom de la racine de mon Dropdown");
        var section1 = new AdvancedDropdownItem("Teleport position");
        foreach(var l in locations)
        {
            var pos1 = new AdvancedDropdownItem(l);
            section1.AddChild(pos1);
        }
        root.AddChild(section1);
        return root;
    }

    protected override void ItemSelected(AdvancedDropdownItem item)
    {
        base.ItemSelected(item);
        Debug.Log(item.name);
    }
}
