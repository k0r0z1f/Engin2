using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEditor;
using UnityEngine;

public class PatrolRoute : MonoBehaviour
{
    [SerializeField] private List<GameObject> positions = new List<GameObject>();
    [SerializeField] private List<string> positionName = new List<string>();
    private GameObject positionsContainer;

    private void Reset()
    {
        if (positionsContainer == null)
        {
            positionsContainer = new GameObject("Positioncontainer of: " + gameObject.name);
        }
    }
    private void AddPosition()
    {
        if(positionsContainer == null) Reset();
        positionName.Add("Set name of Position " + positions.Count);
        positions.Add(new GameObject("Position " + positions.Count));
        positions[positions.Count - 1].transform.SetParent(positionsContainer.transform);
        positions[positions.Count - 1].AddComponent<PickableGizmo>();
        var icon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/cible.png");
        EditorGUIUtility.SetIconForObject(positions[positions.Count - 1], icon);
    }

    private void RemovePosition()
    {
        if (positions.Any())
        {
            var go = positions[positions.Count - 1];
            if (go != null)
            {
                DestroyImmediate(go);
            }
            positions.RemoveAt(positions.Count - 1);
        }
        
        if (positionName.Any())
        {
            positionName.RemoveAt(positionName.Count - 1);
        }
    }
    
    [CustomEditor(typeof(PatrolRoute))]
    public class PatrolRouteEditor : Editor
    {
        private SerializedProperty targetPosition;
        private SerializedProperty targetPositionName;

        private void OnEnable()
        {
            targetPosition = serializedObject.FindProperty("positions");
            targetPositionName = serializedObject.FindProperty("positionName");
        }

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();
            //DrawDefaultInspector();
            PatrolRoute ptrRoute = (PatrolRoute)target;
            GUILayout.Label("Add Positions for the NPC or Ennemy to patrol");
            if (GUILayout.Button("Add a new Position"))
            {
                ptrRoute.AddPosition();
            }
            
            foreach (var p in ptrRoute.positions)
            {
                int index = ptrRoute.positions.FindIndex(x => x == p);
                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();
                ptrRoute.positionName[index] = GUILayout.TextField((ptrRoute.positionName[index]), GUILayout.MinWidth(130));
                if (EditorGUI.EndChangeCheck())
                {
                   // ptrRoute.positionsContainer.transform.GetChild(index).name = ptrRoute.positionName[index];
                    ptrRoute.positions[index].name = ptrRoute.positionName[index];
                }
                GUILayout.Space(10);
                
                EditorGUI.BeginChangeCheck();
                UnityEngine.Vector3 pos = EditorGUILayout.Vector3Field("", ptrRoute.positions[index].transform.position, GUILayout.MinWidth(Screen.width * 0.275f)); //à retravailler 
                if (EditorGUI.EndChangeCheck())
                {
                    ptrRoute.positions[index].transform.position = pos;
                }
                EditorGUILayout.EndHorizontal();
                
                Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(1));
                EditorGUI.DrawRect(r, Color.gray);
                
            }

            if (GUILayout.Button("Remove Last Position"))
            {
                //if(ptrRoute.positions.Count != 0)
                    ptrRoute.RemovePosition();
            }
            
            // EditorGUILayout.PropertyField(targetPositionName, new GUIContent("Location Name"));
            // EditorGUILayout.PropertyField(targetPosition, new GUIContent("Position"));
            serializedObject.ApplyModifiedProperties();
        }
    }

}
