using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    private NavMeshAgent nMAgent;

    [SerializeField] private Vector3 destinationTarget;

    void Awake()
    {
        nMAgent = GetComponent<NavMeshAgent>();
        ChoseDestination();
    }

    void ChoseDestination()
    {
        nMAgent.destination = destinationTarget;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(destinationTarget, "Assets/Gizmos/cible.png");
    }
    
    void FindPosition()
    {
        destinationTarget = Vector3.zero;

        while (destinationTarget == Vector3.zero)
        {
            Vector3 randomPoint = Random.insideUnitSphere * 100 + transform.position;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 100, NavMesh.AllAreas))
            {
                destinationTarget = hit.position;
            }
        }
    }
    
    private void Update()
    {
        if (nMAgent.remainingDistance < 0.2f)
        {
            ChoseDestination();
        }
    }
    
}