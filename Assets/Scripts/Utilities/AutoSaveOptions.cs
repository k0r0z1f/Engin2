using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoSaveOptions : EditorWindow
{

    delegate void SaveOptions();
    private SaveOptions saveOption;

    private int last;
    
    private bool autosave;
    private bool optionTime;
    private bool optionDirty;
    private bool optionPlay;
    private bool optionsSection;
    [MenuItem("Alexis/Autosave")]
    public static void ShowWindow()
    {
        GetWindow(typeof(AutoSaveOptions));
    }

    private void OnGUI()
    {
        autosave = GUILayout.Toggle(autosave, "Autosave Enabled");
        optionsSection =
            EditorGUILayout.BeginFoldoutHeaderGroup(optionsSection, "Select what kind of save you want");
        if (optionsSection)
        {
            if(last == -1)
                EditorGUILayout.HelpBox("Select at least one option", MessageType.Warning, true);
            optionTime = GUILayout.Toggle(optionTime, "Time");
            optionDirty = GUILayout.Toggle(optionDirty, "Dirty");
            optionPlay = GUILayout.Toggle(optionPlay, "Play");

            // if (Selection.activeTransform)
            // {
                if (saveOption != null)
                {
                    saveOption();
                }
                else if(last == -1)
                {
                    Debug.LogWarning("You must select at least one option");
                }
            //}
        }
        
        if(EditorGUI.EndChangeCheck())
        {
            OnUpdateDelegate();
        }

    }

    void OnUpdateDelegate()
    {
        saveOption = null;
        //Debug.Log("optiontime " + optionTime);
        if (optionTime && last != 0)
        {
            //saveOption += OnOptionTime;
            last = 0;
        } else if (optionDirty && last != 1)
        {
            //saveOption += OnOptionDirty;
            last = 1;
        } else if (optionPlay && last != 2)
        {
            //saveOption += OnOptionPlay;
            last = 2;
        }
        else if(!optionTime && !optionDirty && !optionPlay)
        {
            last = -1;
        }

        if (last == 0)
        {
            optionDirty = false;
            optionPlay = false;
            if(autosave)
            {
                saveOption += OnOptionTime;
            }
        }
        else if (last == 1)
        {
            optionTime = false;
            optionPlay = false;
            if (autosave)
            {
                saveOption += OnOptionDirty;
            }
        }
        else if (last == 2)
        {
            optionTime = false;
            optionDirty = false;
            if (autosave)
            {
                saveOption += OnOptionPlay;
            }
        }
    }

    void OnFocus()
    {
        if (EditorPrefs.HasKey("optionsSection"))
            optionsSection = EditorPrefs.GetBool("optionsSection");
        if (EditorPrefs.HasKey("optionTime"))
            optionTime = EditorPrefs.GetBool("optionTime");
        if (EditorPrefs.HasKey("optionDirty"))
            optionDirty = EditorPrefs.GetBool("optionDirty");
        if (EditorPrefs.HasKey("optionPlay"))
            optionPlay = EditorPrefs.GetBool("optionPlay");
    }

    private void OnLostFocus()
    {
        EditorPrefs.SetBool("optionsSection", optionsSection);
        EditorPrefs.SetBool("optionTime", optionTime);
        EditorPrefs.SetBool("optionDirty", optionDirty);
        EditorPrefs.SetBool("optionPlay", optionPlay);
    }

    private void OnDestroy()
    {
        EditorPrefs.SetBool("optionsSection", optionsSection);
        EditorPrefs.SetBool("optionTime", optionTime);
        EditorPrefs.SetBool("optionDirty", optionDirty);
        EditorPrefs.SetBool("optionPlay", optionPlay);
    }
    
    void OnOptionTime()
    {
        /*foreach (var obj in Selection.transforms)
        {
            if (obj.GetComponent<MeshRenderer>())
            {
                var newSize = UnityEngine.Random.Range(1, 5);
                obj.localScale = new Vector3(newSize, newSize, newSize);
            }
            else
            {
                Debug.LogWarning("An object you have selected does not have a MeshRenderer");
            }
        }*/
        Debug.Log("Time");
    }
    
    public void OnOptionDirty()
    {
        EditorApplication.SaveScene();
// #if UNITY_EDITOR
//         EditorApplication.update += MyUpdateMethod;
//         EditorApplication.playModeStateChanged += LogPlayModeState;
//         EditorSceneManager.sceneDirtied += SceneChanged;
// #endif
        // foreach (var obj in Selection.transforms)
        // {
        //     obj.rotation = UnityEngine.Random.rotation;
        // }
        Debug.Log("OnOptionDirty");
    }
    
    void OnOptionPlay()
    {
        /*foreach (var obj in Selection.transforms)
        {
            var renderer = obj.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.sharedMaterial.color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0f, 1f);
            }
            else
            {
                Debug.LogWarning("An object you have selected does not have a Renderer for color");
            }
        }*/
         Debug.Log("OnOptionPlay");
    }
    
    

    static void MyUpdateMethod()
    {
        //Debug.Log("update ");
    }

    private static void LogPlayModeState(PlayModeStateChange state)
    {
        Debug.Log(state);
    }

    private static void SceneChanged(Scene scene)
    {
        Debug.Log("Changement dans la sceneRIGHT NOW");
        EditorApplication.SaveScene();
#if UNITY_EDITOR
        EditorApplication.update -= MyUpdateMethod;
        EditorApplication.playModeStateChanged -= LogPlayModeState;
        EditorSceneManager.sceneDirtied -= SceneChanged;
#endif
    }
    
    
}
