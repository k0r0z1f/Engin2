using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StyleViewer : EditorWindow
{
    private TextAsset myJSONStyles;
    private int selectedStyle;
    private TheEditorStyles editorStyles;
    private float sValue;

    private struct TheEditorStyles
    {
        public string[] EdStyles;
    }
    
    [MenuItem("Window/Alexis/Viewfile System")]
    
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(StyleViewer));
    }

    void OnGUI()
    {
        selectedStyle = EditorGUILayout.Popup("Tous les styles", selectedStyle, editorStyles.EdStyles);
        GUILayout.Space(10f);
        GUILayout.Label("Exemple de Label Stylé", editorStyles.EdStyles[selectedStyle]);
        GUILayout.Space(10f);
        GUILayout.Box("Exemple de Box");
        GUILayout.Space(10f);
        GUILayout.Button("Exemple de Bouton");
        GUILayout.Space(10f);
        GUILayout.Toggle(true, "Exemple de Toogle");
        GUILayout.Space(10f);
        sValue = GUILayout.HorizontalScrollbar(sValue, 1f, 0f, 10f);
    }

    private void OnEnable()
    {
        myJSONStyles = Resources.Load<TextAsset>("UnityStyles");
        editorStyles = JsonUtility.FromJson<TheEditorStyles>(myJSONStyles.text);
    }
}
