using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Randomizer : EditorWindow
{

    delegate void OptionsToRandomize();
    private OptionsToRandomize optionToRandomize;
    
    private bool rdnSize;
    private bool rdnRotation;
    private bool rdnColor;
    private bool randomizerSection;
    [MenuItem("Alexis/Randomizer")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(Randomizer));
    }

    private void OnGUI()
    {
        randomizerSection =
            EditorGUILayout.BeginFoldoutHeaderGroup(randomizerSection, "Select the options to randomize");
        if (randomizerSection)
        {
            if(optionToRandomize == null)
                EditorGUILayout.HelpBox("Select at least one option", MessageType.Warning, true);
            rdnSize = GUILayout.Toggle(rdnSize, "Size");
            rdnRotation = GUILayout.Toggle(rdnRotation, "Rotation");
            rdnColor = GUILayout.Toggle(rdnColor, "Color");

            if (Selection.activeTransform != null)
            {
                if (GUILayout.Button("Randomize!"))
                {
                    if (optionToRandomize != null)
                    {
                        optionToRandomize();
                    }
                    else
                    {
                        Debug.LogWarning("You must select at least one option");
                    }
                }
            }
        }
        
        if(EditorGUI.EndChangeCheck())
        {
            OnUpdateDelegate();
        }

    }

    void OnUpdateDelegate()
    {
        optionToRandomize = null;
        if (rdnSize) optionToRandomize += OnRandomSize;
        if (rdnRotation) optionToRandomize += OnRandomRotation;
        if (rdnColor) optionToRandomize += OnRandomColor;
    }

    void OnFocus()
    {
        if (EditorPrefs.HasKey("randomizerSection"))
            randomizerSection = EditorPrefs.GetBool("randomizerSection");
        if (EditorPrefs.HasKey("rdnSize"))
            rdnSize = EditorPrefs.GetBool("rdnSize");
        if (EditorPrefs.HasKey("rdnRotation"))
            rdnRotation = EditorPrefs.GetBool("rdnRotation");
        if (EditorPrefs.HasKey("rdnColor"))
            rdnColor = EditorPrefs.GetBool("rdnColor");
    }

    private void OnLostFocus()
    {
        EditorPrefs.SetBool("randomizerSection", randomizerSection);
        EditorPrefs.SetBool("rdnSize", rdnSize);
        EditorPrefs.SetBool("rdnRotation", rdnRotation);
        EditorPrefs.SetBool("rdnColor", rdnColor);
    }

    private void OnDestroy()
    {
        EditorPrefs.SetBool("randomizerSection", randomizerSection);
        EditorPrefs.SetBool("rdnSize", rdnSize);
        EditorPrefs.SetBool("rdnRotation", rdnRotation);
        EditorPrefs.SetBool("rdnColor", rdnColor);
    }
    
    void OnRandomSize()
    {
        foreach (var obj in Selection.transforms)
        {
            if (obj.GetComponent<MeshRenderer>())
            {
                var newSize = UnityEngine.Random.Range(1, 5);
                obj.localScale = new Vector3(newSize, newSize, newSize);
            }
            else
            {
                Debug.LogWarning("An object you have selected does not have a MeshRenderer");
            }
        }
        //   Debug.Log("Random Size");
    }
    
    void OnRandomRotation()
    {
        foreach (var obj in Selection.transforms)
        {
            obj.rotation = UnityEngine.Random.rotation;
        }
        //Debug.Log("Random Rotation");
    }
    
    void OnRandomColor()
    {
        foreach (var obj in Selection.transforms)
        {
            var renderer = obj.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.sharedMaterial.color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0f, 1f);
            }
            else
            {
                Debug.LogWarning("An object you have selected does not have a Renderer for color");
            }
        }
        // Debug.Log("Random Color");
    }
}
