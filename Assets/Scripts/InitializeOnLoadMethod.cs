using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class InitializeOnLoadMethod
{
    static InitializeOnLoadMethod()
    {
        //Debug.Log("allo");
#if UNITY_EDITOR
        EditorApplication.update += MyUpdateMethod;
        EditorApplication.playModeStateChanged += LogPlayModeState;
        EditorSceneManager.sceneDirtied += SceneChanged;
#endif
    }

    static void MyUpdateMethod()
    {
        //Debug.Log("update ");
    }

    private static void LogPlayModeState(PlayModeStateChange state)
    {
        Debug.Log(state);
    }

    private static void SceneChanged(Scene scene)
    {
        Debug.Log("Changement dans la scene");
        //AutoSaveOptions
    }
}
